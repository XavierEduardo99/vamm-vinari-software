/*
	Copyright (c) 2015 - 2025, Vinari Software
	All rights reserved.

	Redistribution and use in source and binary forms, with or without
	modification, are permitted provided that the following conditions are met:

	1. Redistributions of source code must retain the above copyright notice, this
	   list of conditions and the following disclaimer.

	2. Redistributions in binary form must reproduce the above copyright notice,
	   this list of conditions and the following disclaimer in the documentation
	   and/or other materials provided with the distribution.

	3. Neither the name of the copyright holder nor the names of its
	   contributors may be used to endorse or promote products derived from
	   this software without specific prior written permission.

	THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
	AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
	IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
	DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
	FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
	DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
	SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
	CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
	OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
	OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/


namespace UI {
	[GtkTemplate (ui="/org/vinarisoftware/vamm/UI/MainWindow.ui")]
	public class MainWindow : Gtk.ApplicationWindow {
		[GtkChild] private unowned Gtk.HeaderBar headerBar;
		[GtkChild] private unowned Gtk.Box mainBox;

		private GLib.TimeZone currentTimeZone=new GLib.TimeZone.local();
		private Gtk.Label webServerLabel;
		private Gtk.Label databaseLabel;

		private Gtk.Image webServerStatusImage;
		private Gtk.Label webServerStatusLabel;
		private Gtk.Image databaseStatusImage;
		private Gtk.Label databaseStatusLabel;

		private Gtk.Image webServerRestartImage;
		private Gtk.Image databaseRestartImage;

		private Gtk.Button webServerStartButton;
		private Gtk.Button webServerStopButton;
		private Gtk.Button databaseStartButton;
		private Gtk.Button databaseStopButton;

		private Gtk.Button webServerRestartButton;
		private Gtk.Button databaseRestartButton;

		private Gtk.ScrolledWindow logScrolledWindow;
		private Gtk.TextView logTextView;

		private Gtk.Button serverRootButton;
		private Gtk.Button ifconfigButton;

		private GLib.Settings mySettings;
		private string webServerName;
		private string dbmsName;
		private int enabledStatus;
		private App.Widgets.MessageDialog messageDialog;

		public MainWindow (Gtk.Application app) {
			Object (application: app);
		}

		public void updateWebServerName(){
			this.webServerName=mySettings.get_string("web-server");
		}

		public void updateDatabaseName(){
			this.dbmsName=mySettings.get_string("dbms");
		}

		construct{
			this.set_title("VAMM - Vinari Software");
			//this.set_resizable(false);
			this.mainBox.set_orientation(Gtk.Orientation.VERTICAL);
			mySettings=new GLib.Settings("org.vinarisoftware.vamm");

			this.updateWebServerName();
			this.updateDatabaseName();

			// Main labels for defining the row
			webServerLabel=new Gtk.Label(_("Web server status control"));
			databaseLabel=new Gtk.Label(_("Database engine status control"));

			// Label and image to show the status of the web server service
			webServerStatusImage=new Gtk.Image.from_icon_name("dialog-close");
			webServerStatusLabel=new Gtk.Label(_("Current status: Inactive"));

			// Label and image to show the status of the database service
			databaseStatusImage=new Gtk.Image.from_icon_name("dialog-close");
			databaseStatusLabel=new Gtk.Label(_("Current status: Inactive"));

			// Definition of the button (In the HeaderBar) that opens the server's root folder
			serverRootButton=new Gtk.Button.from_icon_name("folder-publicshare-symbolic");
			serverRootButton.set_tooltip_text(_("Open the server's public directory"));
			serverRootButton.clicked.connect(serverRootButtonAction);

			// Definition of the button (In the HeaderBar) that runs the 'ifconfig' command
			ifconfigButton=new Gtk.Button.from_icon_name("network-wired-no-route-symbolic");
			ifconfigButton.set_tooltip_text(_("Run the 'ifconfig' command"));
			ifconfigButton.clicked.connect(ifconfigButtonAction);

			// Definition of the Gtk.Box that will be placed inside the HeaderBar wit the previous buttons
			Gtk.Box headerBox=new Gtk.Box(Gtk.Orientation.HORIZONTAL, 3);
			headerBox.append(serverRootButton);
			headerBox.append(ifconfigButton);
			headerBar.pack_start(headerBox);

			// Control buttons for the web server are created
			webServerStartButton=new Gtk.Button();
			webServerStartButton.set_hexpand(true);
			webServerStopButton=new Gtk.Button();
			webServerStopButton.set_hexpand(true);

			// Labels that will be inside the control buttons for the web server are created
			Gtk.Label webServerStartButtonLabel=new Gtk.Label(_("Start web server"));
			Gtk.Label webServerStopButtonLabel=new Gtk.Label(_("Stop web server"));

			// Images that will be insite the control buttons for the web server are created
			Gtk.Image webServerStartButtonImage=new Gtk.Image.from_icon_name("system-run-symbolic");
			Gtk.Image webServerStopButtonImage=new Gtk.Image.from_icon_name("process-stop-symbolic");

			// Boxes that will be insite the control buttons for the web server are created
			Gtk.Box webServerStartButtonBox=new Gtk.Box(Gtk.Orientation.HORIZONTAL, 5);
			Gtk.Box webServerStopButtonBox=new Gtk.Box(Gtk.Orientation.HORIZONTAL, 5);

			// The image and label are set on the box that will go on the button that starts the server
			webServerStartButtonBox.append(webServerStartButtonImage);
			webServerStartButtonBox.append(webServerStartButtonLabel);
			webServerStartButtonBox.set_halign(CENTER);				// Sets the box contents to the center

			webServerStartButton.set_child(webServerStartButtonBox);

			// The image and label are set on the box that will go on the button that stops the server
			webServerStopButtonBox.append(webServerStopButtonImage);
			webServerStopButtonBox.append(webServerStopButtonLabel);
			webServerStopButtonBox.set_halign(CENTER);				// Sets the box contents to the center

			webServerStopButton.set_child(webServerStopButtonBox);

			// Control buttons for the database engine are created
			databaseStartButton=new Gtk.Button();
			databaseStartButton.set_hexpand(true);
			databaseStopButton=new Gtk.Button();
			databaseStopButton.set_hexpand(true);

			// Labels that will be inside the control buttons for the database engine are created
			Gtk.Label databaseStartButtonLabel=new Gtk.Label(_("Start database engine"));
			Gtk.Label databaseStopButtonLabel=new Gtk.Label(_("Stop database engine"));

			// Images that will be inside the control buttons for the database engine are created
			Gtk.Image databaseStartButtonImage=new Gtk.Image.from_icon_name("system-run-symbolic");
			Gtk.Image databaseStopButtonImage=new Gtk.Image.from_icon_name("process-stop-symbolic");

			// Boxes that will be inside the control buttons for the database engine are created
			Gtk.Box databaseStartButtonBox=new Gtk.Box(Gtk.Orientation.HORIZONTAL, 5);
			Gtk.Box databaseStopButtonBox=new Gtk.Box(Gtk.Orientation.HORIZONTAL, 5);

			// The image and label are set on the box that will go on the button that starts the database engine
			databaseStartButtonBox.append(databaseStartButtonImage);
			databaseStartButtonBox.append(databaseStartButtonLabel);
			databaseStartButtonBox.set_halign(CENTER);				// Sets the box contents to the center

			databaseStartButton.set_child(databaseStartButtonBox);

			// The image and label are set on the box that will go on the button that stops the database engine
			databaseStopButtonBox.append(databaseStopButtonImage);
			databaseStopButtonBox.append(databaseStopButtonLabel);
			databaseStopButtonBox.set_halign(CENTER);				// Sets the box contents to the center

			databaseStopButton.set_child(databaseStopButtonBox);

			// Initialization of the buttons to restart the web server and the database services
			webServerRestartButton=new Gtk.Button();
			databaseRestartButton=new Gtk.Button();

			// A tooltip text is added to the buttons used for restarting the services
			webServerRestartButton.set_tooltip_text(_("Restart the web server service"));
			databaseRestartButton.set_tooltip_text(_("Restart the database service"));

			// Initialization of the images that will be shown in the buttons to restart the web server and the database services
			webServerRestartImage=new Gtk.Image.from_icon_name("view-refresh-symbolic");
			databaseRestartImage=new Gtk.Image.from_icon_name("view-refresh-symbolic");

			// The images are set to the buttons for restarting the services
			webServerRestartButton.set_child(webServerRestartImage);
			databaseRestartButton.set_child(databaseRestartImage);

			Gtk.Box webServerBox=new Gtk.Box(Gtk.Orientation.HORIZONTAL, 10);
			Gtk.Box databaseBox=new Gtk.Box(Gtk.Orientation.HORIZONTAL, 10);

			logScrolledWindow=new Gtk.ScrolledWindow();
			logTextView=new Gtk.TextView();
			logTextView.set_editable(false);
			logTextView.set_monospace(true);

			sendLog(_("Sarting VAMM..."));

			logScrolledWindow.set_child(logTextView);
			logScrolledWindow.set_vexpand(true);
			logScrolledWindow.set_hexpand(true);

			webServerBox.append(webServerStatusImage);
			webServerBox.append(webServerStatusLabel);
			webServerBox.append(webServerStopButton);
			webServerBox.append(webServerStartButton);
			webServerBox.append(webServerRestartButton);

			databaseBox.append(databaseStatusImage);
			databaseBox.append(databaseStatusLabel);
			databaseBox.append(databaseStopButton);
			databaseBox.append(databaseStartButton);
			databaseBox.append(databaseRestartButton);

			this.mainBox.append(webServerLabel);
			this.mainBox.append(webServerBox);

			this.mainBox.append(databaseLabel);
			this.mainBox.append(databaseBox);
			this.mainBox.append(logScrolledWindow);

			// Signals for the buttons that control the web server service
			webServerStartButton.clicked.connect(webServerStartButtonAction);
			webServerStopButton.clicked.connect(webServerStopButtonAction);
			webServerRestartButton.clicked.connect(webServerRestartButtonAction);

			// Signals for the buttons that control the database service
			databaseStartButton.clicked.connect(databaseStartButtonAction);
			databaseStopButton.clicked.connect(databaseStopButtonAction);
			databaseRestartButton.clicked.connect(databaseRestartButtonAction);

			enabledStatus=checkActiveService(this.webServerName);
			sendLog(_("Checking if the '")+this.webServerName+_("' service is active. Exit code: ")+enabledStatus.to_string());
			if(enabledStatus==0){
				this.setActiveWebServer();
			}else{
				this.setInactiveWebServer();
			}

			enabledStatus=checkActiveService(this.dbmsName);
			sendLog(_("Checking if the '")+this.dbmsName+_("' service is active. Exit code: ")+enabledStatus.to_string());
			if(enabledStatus==0){
				this.setActiveDatabase();
			}else{
				this.setInactiveDatabase();
			}

			sendLog(_("VAMM has starded..."));
		}

		private int checkActiveService(string serviceToCheck){
			return Posix.system("/usr/bin/systemctl is-active " + serviceToCheck + " >/dev/null 2>&1");
		}

		private int runService(string serviceToRun){
			return Posix.system("/usr/bin/systemctl start " + serviceToRun);
		}

		private int stopService(string serviceToStop){
			return Posix.system("/usr/bin/systemctl stop " + serviceToStop);
		}

		private int restartService(string serviceToRestart){
			return Posix.system("/usr/bin/systemctl restart " + serviceToRestart);
		}

		public void sendLog(string logToSend){
			GLib.DateTime currentTime=new GLib.DateTime.now(currentTimeZone);				//Gets the current time and time zone
			string currentTimeS=currentTime.format("[%H:%M:%S - %b %eth %Y] ");				//Formats the current time and returns a string
			string currentLog=logTextView.buffer.text;

			currentLog+=currentTimeS+logToSend+"\n";
			this.logTextView.buffer.text=currentLog;
		}

		private void unknownStatusServer(int exitCode, string actionPerformed){
			this.sendLog(_("An error occurred when ") + actionPerformed + _(" the '") + this.webServerName + _("' service. Exit code: ") + exitCode.to_string() + ".");
			this.webServerStatusImage.set_from_icon_name("dialog-question-symbolic");
			this.webServerStatusLabel.set_text(_("Current status: Unknown"));

			this.webServerStopButton.set_sensitive(true);
			this.webServerStartButton.set_sensitive(true);
		}

		private void unknownStatusDatabase(int exitCode, string actionPerformed){
			this.sendLog(_("An error occurred when ") + actionPerformed + _(" the '") + this.dbmsName + _("' service. Exit code: ") + exitCode.to_string() + ".");
			this.databaseStatusImage.set_from_icon_name("dialog-question-symbolic");
			this.databaseStatusLabel.set_text(_("Current status: Unknown"));

			this.databaseStartButton.set_sensitive(true);
			this.databaseStopButton.set_sensitive(true);
		}

		private void setActiveWebServer(){
			webServerStatusImage.set_from_icon_name("dialog-apply");
			webServerStatusLabel.set_text(_("Current status: Active"));

			webServerStartButton.set_sensitive(false);
			webServerStopButton.set_sensitive(true);
		}

		private void setInactiveWebServer(){
			webServerStatusImage.set_from_icon_name("dialog-close");
			webServerStatusLabel.set_text(_("Current status: Inactive"));

			webServerStartButton.set_sensitive(true);
			webServerStopButton.set_sensitive(false);
		}

		private void setActiveDatabase(){
			databaseStatusImage.set_from_icon_name("dialog-apply");
			databaseStatusLabel.set_text(_("Current status: Active"));

			databaseStartButton.set_sensitive(false);
			databaseStopButton.set_sensitive(true);
		}

		private void setInactiveDatabase(){
			databaseStatusImage.set_from_icon_name("dialog-close");
			databaseStatusLabel.set_text(_("Current status: Inactive"));

			databaseStartButton.set_sensitive(true);
			databaseStopButton.set_sensitive(false);
		}

		private void webServerStartButtonAction(){
			this.updateWebServerName();

			int startCode=runService(this.webServerName);
			if(startCode==0){
				this.setActiveWebServer();
				sendLog(_("The service '") + this.webServerName + _("' has starded. Exit code: ") + startCode.to_string());
			}else{
				this.unknownStatusServer(startCode, _("starting"));
			}
		}

		private void webServerStopButtonAction(){
			this.updateWebServerName();

			int stopCode=stopService(this.webServerName);
			if(stopCode==0){
				this.setInactiveWebServer();
				sendLog(_("The service '") + this.webServerName + _("' has stopped. Exit code: ") + stopCode.to_string());
			}else{
				this.unknownStatusServer(stopCode, _("stopping"));

			}
		}

		private void webServerRestartButtonAction(){
			this.updateWebServerName();

			int stopCode=restartService(this.webServerName);
			if(stopCode==0){
				this.setActiveWebServer();
				sendLog(_("The service '") + this.webServerName + _("' has been restarted. Exit code: ") + stopCode.to_string());
			}else{
				this.unknownStatusServer(stopCode, _("stopping"));
			}
		}

		private void databaseStartButtonAction(){
			this.updateDatabaseName();

			int startCode=runService(this.dbmsName);
			if(startCode==0){
				this.setActiveDatabase();
				sendLog(_("The service '") + this.dbmsName + _("' has starded. Exit code: ") + startCode.to_string());
			}else{
				this.unknownStatusDatabase(startCode, _("starting"));
			}
		}

		private void databaseStopButtonAction(){
			this.updateDatabaseName();

			int stopCode=stopService(this.dbmsName);
			if(stopCode==0){
				this.setInactiveDatabase();
				sendLog(_("The service '") + this.dbmsName + _("' has stopped. Exit code: ") + stopCode.to_string());
			}else{
				this.unknownStatusDatabase(stopCode, _("stopping"));
			}
		}

		private void databaseRestartButtonAction(){
			this.updateDatabaseName();

			int stopCode=restartService(this.dbmsName);
			if(stopCode==0){
				this.setActiveDatabase();
				sendLog(_("The service '") + this.dbmsName + _("' has been restarted. Exit code: ") + stopCode.to_string());
			}else{
				this.unknownStatusDatabase(stopCode, _("stopping"));
			}
		}

		private void ifconfigButtonAction(){
			string ls_stdout;
			string ls_stderr;
			int ls_status;

			try{
				Process.spawn_command_line_sync ("ifconfig", out ls_stdout, out ls_stderr, out ls_status);
				sendLog("\n\n" + ls_stdout);
			}catch(SpawnError e){
				messageDialog=new App.Widgets.MessageDialog(this, ERROR, OK, _("An unexpected error occurred while trying to execute the 'ifconfig' command.\nIs it possible that you do not have enough permissions? \n'") + e.message + "'");
				sendLog("CLI Sync error: " + e.message);
			}
		}

		private void serverRootButtonAction(){
			try{
				sendLog(_("Trying to open the server's root folder at: '") + this.mySettings.get_string("server-root") + "'");
				Process.spawn_command_line_async("xdg-open "+this.mySettings.get_string("server-root"));
			} catch (SpawnError e) {
				messageDialog=new App.Widgets.MessageDialog(this, ERROR, OK, _("An unexpected error occurred while trying to open the server's public directory.\nIs it possible that this directory does not exist? \n'") + e.message + "'");
				sendLog("CLI Async error: " + e.message);
			}
		}
	}
}
