/*
    Copyright (c) 2015 - 2025, Vinari Software
    All rights reserved.

    Redistribution and use in source and binary forms, with or without
    modification, are permitted provided that the following conditions are met:

    1. Redistributions of source code must retain the above copyright notice, this
       list of conditions and the following disclaimer.

    2. Redistributions in binary form must reproduce the above copyright notice,
       this list of conditions and the following disclaimer in the documentation
       and/or other materials provided with the distribution.

    3. Neither the name of the copyright holder nor the names of its
       contributors may be used to endorse or promote products derived from
       this software without specific prior written permission.

    THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
    AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
    IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
    DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
    FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
    DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
    SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
    CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
    OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
    OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/

namespace UI{
	public class SettingsWindow : Gtk.Window {
		private Gtk.Label databaseNameLabel;
		private Gtk.Label webServerNameLabel;
		private Gtk.Label webServerRootLabel;

		private Gtk.Entry databaseNameEntry;
		private Gtk.Entry webServerNameEntry;
		private Gtk.Entry webServerRootEntry;

		private Gtk.Label webServerServiceLabel;
		private Gtk.Label databaseServiceLabel;
		private Gtk.Switch webServerServiceSwitch;
		private Gtk.Switch databaseServiceSwitch;

		private Gtk.Button acceptButton;
		private Gtk.Button cancelButton;
		private GLib.Settings mySettings;

		private Regex mainRegex;
		private App.Widgets.MessageDialog messageDialog;

		private bool webServerServiceSwitchStatus;
		private bool databaseServiceSwitchStatus;

		public SettingsWindow(Gtk.Window parent){
			this.set_transient_for(parent);
			this.set_modal(true);
			this.set_default_size(360, 380);
			this.set_resizable(false);
			this.set_icon_name("org.vinarisoftware.integritycheck");
			this.set_title(_("VAMM - Vinari Software | Preferences"));
		}

		private bool checkServicesEnabled(string serviceToCheck){
			int exitCode = Posix.system("/usr/bin/systemctl is-enabled " + serviceToCheck);
			if(exitCode==0){
				return true;
			}

			return false;
		}

		construct{
			Gtk.Box mainBox=new Gtk.Box(Gtk.Orientation.VERTICAL, 15);
			Gtk.Box actionButtonsBox=new Gtk.Box(Gtk.Orientation.HORIZONTAL, 15);
			this.mySettings=new GLib.Settings("org.vinarisoftware.vamm");

			try{
				this.mainRegex=new Regex("^[a-zA-Z0-9/]+$");
			}catch(GLib.RegexError e){
				print("ERROR: %s", e.message);
				messageDialog=new App.Widgets.MessageDialog(this, ERROR, OK, _("An unexpected error occurred while trying to set the default validator for the services... \nProceed carefuly"));
			}

			// Sets the margin for the mainBox in relation to the window borders
			mainBox.set_margin_top(15);
			mainBox.set_margin_bottom(15);
			mainBox.set_margin_start(15);
			mainBox.set_margin_end(15);

			// Initialices the labels and their default position
			databaseNameLabel=new Gtk.Label(_("Database engine provider:"));
			databaseNameLabel.set_halign(START);
			webServerNameLabel=new Gtk.Label(_("Web server provider:"));
			webServerNameLabel.set_halign(START);
			webServerRootLabel=new Gtk.Label(_("Web server's public folder"));
			webServerRootLabel.set_halign(START);

			// Initialices the entry that will hold the database engine name
			databaseNameEntry=new Gtk.Entry();
			databaseNameEntry.set_text(this.mySettings.get_string("dbms"));

			// Initialices the entry that will hold the web server name
			webServerNameEntry=new Gtk.Entry();
			webServerNameEntry.set_text(this.mySettings.get_string("web-server"));

			// Initialices the entry that will hold the web server's public folder
			webServerRootEntry=new Gtk.Entry();
			webServerRootEntry.set_text(this.mySettings.get_string("server-root"));

			// Initialices and sets the position of the labels that indicate the state of the SystemD services
			webServerServiceLabel=new Gtk.Label(_("Web server enabled at boot: "));
			databaseServiceLabel=new Gtk.Label(_("Database engine enabled at boot:"));
			webServerServiceLabel.set_halign(START);
			databaseServiceLabel.set_halign(START);

			// Initialices the switches that will show the state of the SystemD services
			webServerServiceSwitch=new Gtk.Switch();
			databaseServiceSwitch=new Gtk.Switch();
			webServerServiceSwitch.set_active(checkServicesEnabled(this.mySettings.get_string("web-server")));
			webServerServiceSwitchStatus=webServerServiceSwitch.get_active();

			databaseServiceSwitch.set_active(checkServicesEnabled(this.mySettings.get_string("dbms")));
			databaseServiceSwitchStatus=databaseServiceSwitch.get_active();

			// Initialices the action buttons (Cancel and accept)
			acceptButton=new Gtk.Button();
			acceptButton.set_hexpand(true);
			acceptButton.clicked.connect(acceptButtonAction);

			cancelButton=new Gtk.Button();
			cancelButton.set_hexpand(true);
			cancelButton.clicked.connect(cancelButtonAction);

			// Creates the boxes that will hold the SystemD services
			Gtk.Box webServerServiceBox=new Gtk.Box(Gtk.Orientation.HORIZONTAL, 85);
			Gtk.Box databaseServiceBox=new Gtk.Box(Gtk.Orientation.HORIZONTAL, 62);

			// Creates the boxes that will hold the server and database names
			Gtk.Box databaseNameBox=new Gtk.Box(Gtk.Orientation.VERTICAL, 8);
			Gtk.Box webServerNameBox=new Gtk.Box(Gtk.Orientation.VERTICAL, 8);
			Gtk.Box webServerRootBox=new Gtk.Box(Gtk.Orientation.VERTICAL, 8);

			// Creates the boxes that will bo inside the action buttons
			Gtk.Box acceptButtonBox=new Gtk.Box(Gtk.Orientation.HORIZONTAL, 10);
			Gtk.Box cancelButtonBox=new Gtk.Box(Gtk.Orientation.HORIZONTAL, 10);

			// Images and labels that will go inside the action buttons
			Gtk.Image acceptButtonImage=new Gtk.Image.from_icon_name("emblem-ok-symbolic");
			Gtk.Label acceptButtonLabel=new Gtk.Label(_("Accept"));

			Gtk.Image cancelButtonImage=new Gtk.Image.from_icon_name("action-unavailable-symbolic");
			Gtk.Label cancelButtonLabel=new Gtk.Label(_("Cancel"));

			// The position of the elements for the 'Accept' buttons are placed
			acceptButtonBox.append(acceptButtonImage);
			acceptButtonBox.append(acceptButtonLabel);
			acceptButtonBox.set_halign(CENTER);

			// The position of the elements for the 'Cancel' buttons are placed
			cancelButtonBox.append(cancelButtonImage);
			cancelButtonBox.append(cancelButtonLabel);
			cancelButtonBox.set_halign(CENTER);

			// The previous labels are set in their respective buttons
			acceptButton.set_child(acceptButtonBox);
			cancelButton.set_child(cancelButtonBox);

			// The action buttons are placed in their respective box
			actionButtonsBox.append(cancelButton);
			actionButtonsBox.append(acceptButton);

			// The label and switch that show the SystemD services are placed in their respective box (Web server)
			webServerServiceBox.append(webServerServiceLabel);
			webServerServiceBox.append(webServerServiceSwitch);
			webServerServiceLabel.set_hexpand(true);

			// The label and switch that show the SystemD services are placed in their respective box (Database Engine)
			databaseServiceBox.append(databaseServiceLabel);
			databaseServiceBox.append(databaseServiceSwitch);
			databaseServiceLabel.set_hexpand(true);

			// The label and entry for the database engine name are placed in their box
			databaseNameBox.append(databaseNameLabel);
			databaseNameBox.append(databaseNameEntry);
			mainBox.append(databaseNameBox);

			// The label and entry for the web server name are placed in their box
			webServerNameBox.append(webServerNameLabel);
			webServerNameBox.append(webServerNameEntry);
			mainBox.append(webServerNameBox);

			// The label and entry for the web server's root are placed in their box
			webServerRootBox.append(webServerRootLabel);
			webServerRootBox.append(webServerRootEntry);
			mainBox.append(webServerRootBox);

			// A separator is added to the window
			Gtk.Separator separatorI=new Gtk.Separator(Gtk.Orientation.HORIZONTAL);
			mainBox.append(separatorI);

			// The box that shows the state of the SystemD services is added to the mainBox
			mainBox.append(webServerServiceBox);
			mainBox.append(databaseServiceBox);

			// The box that holds the action buttons is added to the mainBox
			mainBox.append(actionButtonsBox);
			this.set_child(mainBox);
		}

		public void run(){
			this.present();
		}

		private void cancelButtonAction(){
			this.destroy();
		}

		private int enableService(string serviceToEnable){
			return Posix.system("/usr/bin/systemctl enable " + serviceToEnable);
		}

		private int disableService(string serviceToDisable){
			return Posix.system("/usr/bin/systemctl disable " + serviceToDisable);
		}

		private void acceptButtonAction(){
			// Get the values set for the entries
			string webServerToSet=this.webServerNameEntry.get_text();
			string databaseToSet=this.databaseNameEntry.get_text();
			string webServerRootToSet=this.webServerRootEntry.get_text();

			// Checks that the name of the web server, database and server root follow the regex rules
			bool okWebserver=mainRegex.match(webServerToSet);
			bool okDatabase=mainRegex.match(databaseToSet);
			bool okServerRoot=mainRegex.match(webServerRootToSet);

			if(okWebserver && okDatabase && okServerRoot){
				this.mySettings.set_string("web-server", webServerToSet);
				this.mySettings.set_string("dbms", databaseToSet);
				this.mySettings.set_string("server-root", webServerRootToSet);
			}else{
				messageDialog=new App.Widgets.MessageDialog(this, WARNING, OK, _("The name of the server, database system and server's root directory cannot contain special characters or be empty."));
				return;
			}

			// If the initial status of the switch that controls the activation of the web server is different to the closing status
			if(webServerServiceSwitchStatus!=webServerServiceSwitch.get_active()){
				int serverStatusExitCode=-9;

				if(webServerServiceSwitch.get_active()){											// If the switch is on, the web server service gets enabled
					serverStatusExitCode=enableService(this.mySettings.get_string("web-server"));
				}else{																				// If the switch is off, the web server service gets disabled
					serverStatusExitCode=disableService(this.mySettings.get_string("web-server"));
				}

				// If a problem is reported after enabling/disabling the web server service
				if(serverStatusExitCode!=0){
					messageDialog=new App.Widgets.MessageDialog(this, WARNING, OK, _("An unexpected error occurred while trying to update the boot status of the web server service."));
					return;
				}
			}

			// If the initial status of the switch that controls the activation of the database is different to the closing status
			if(databaseServiceSwitchStatus!=databaseServiceSwitch.get_active()){
				int databaseStatusExitCode=-9;

				if(databaseServiceSwitch.get_active()){												// If the switch is on, the database service gets enabled
					databaseStatusExitCode=enableService(this.mySettings.get_string("dbms"));
				}else{																				// If the switch is off, the database service gets disabled
					databaseStatusExitCode=disableService(this.mySettings.get_string("dbms"));
				}

				// If a problem is reported after enabling/disabling the database service
				if(databaseStatusExitCode!=0){
					messageDialog=new App.Widgets.MessageDialog(this, WARNING, OK, _("An unexpected error occurred while trying to update the boot status of the database engine service."));
					return;
				}
			}

			this.destroy();
		}
	}
}