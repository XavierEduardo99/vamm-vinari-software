/*
    Copyright (c) 2015 - 2025, Vinari Software
    All rights reserved.

    Redistribution and use in source and binary forms, with or without
    modification, are permitted provided that the following conditions are met:

    1. Redistributions of source code must retain the above copyright notice, this
       list of conditions and the following disclaimer.

    2. Redistributions in binary form must reproduce the above copyright notice,
       this list of conditions and the following disclaimer in the documentation
       and/or other materials provided with the distribution.

    3. Neither the name of the copyright holder nor the names of its
       contributors may be used to endorse or promote products derived from
       this software without specific prior written permission.

    THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
    AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
    IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
    DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
    FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
    DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
    SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
    CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
    OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
    OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/

namespace UI{
	public class AboutWindow {

		private Gtk.AboutDialog mainInstance;				//Since inheritence has been disabled, this is the main instance of the Gtk.AboutDialog

		public AboutWindow(Gtk.Window parent){
			mainInstance=new Gtk.AboutDialog();
			mainInstance.set_license_type(Gtk.License.BSD_3);
			mainInstance.set_copyright("Copyright 2015 - 2025 Vinari Software");
			mainInstance.set_website("https://vinarisoftware.wixsite.com/vinari/");
			mainInstance.set_website_label("Vinari Software");
			mainInstance.set_version("1.2.2 [GTK 4]");
			mainInstance.set_program_name("VAMM - Vinari Software");
			mainInstance.set_logo_icon_name("org.vinarisoftware.vamm");
			mainInstance.set_comments(_("The Vinari OS web server and database system manager,  VAMM- Vinari Software, is an application that allows the user to start and stop LAMP services, therefore, making web development easier."));

			mainInstance.set_artists({"Vinari Software"});
			mainInstance.set_authors({"Vinari Software"});
			mainInstance.set_documenters({"Vinari Software"});
			mainInstance.set_translator_credits("Ensligh - Vinari Software\nEspañol - Vinari Software");

			mainInstance.set_transient_for(parent);
			mainInstance.set_modal(true);
		}

		public void run(){
			this.mainInstance.present();
		}
	}
}